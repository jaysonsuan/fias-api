<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

# Index
Route::get('/', function () {
    return response('FIAS REST API is running...');
});

# Get all workout classes
Route::get('/user/class', function () {
    $classes = DB::table('class as c')
                    ->join('trainer as t', 't.id', '=', 'c.trainer_id')
                    ->select('c.id', 'class_name', 'class_desc', 'start_time', 'end_time', 'day', 'trainer_id',
                        DB::raw("CONCAT(trainer_fname, ' ', trainer_lname) as trainer_name"))
                    ->where([
                        ['c.status', '=', 1],
                        ['t.status', '=', 1]
                    ])
                    ->where('c.status', 1)
                    ->get()->toArray();

    if ($classes) {
        $resp = response()->json(array_map(function ($c) {
            return array(
                'id' => $c->id,
                'name' => $c->class_name,
                'desc' => $c->class_desc,
                'day' => $c->day,
                'start_time' => $c->start_time,
                'end_time' => $c->end_time,
                'trainer' => array(
                    'id' => $c->trainer_id,
                    'name' => $c->trainer_name
                )
            );
        }, $classes));
    } else {
        $resp = response()->json([], 404);
    }

    return $resp;
});

# Get all exercises
Route::get('/user/exercises', function () {
    $exercises = DB::table('exercises')
                    ->select('workout_type', 'name', 'muscle_targeted', 'equipment_type', 'image', 'instructions')
                    ->where('status', 1)
                    ->get()->toArray();

    return response()->json([
        'full_body' => ['Arm', 'Butt', 'Core', 'Leg', 'Back', 'Chest', 'Hips'],
        'cardio' => ['Cardio'],
        'dieting' => ['Arm', 'Back', 'Hips', 'Leg', 'Core'],
        'exercises' => $exercises
    ]);
});

# Get user
Route::get('/user/{id}', function (int $id) {
    $user = DB::table('accounts')
                ->select('id', 'user_fname', 'user_lname', 'address', 'birthdate', 'gender', 'email',
                         'contact', 'weight', 'height', 'img', 'is_verified', 'religion', 'complication')
                ->where('id', $id)
                ->first();
    
    if ($user) {
        $user->complication = $user->complication ? explode('|', $user->complication) : [];
        $resp = response()->json($user);
    } else {
        $resp = response()->json([], 404);
    }
    
    return $resp;
});

# Update user
Route::post('/user/{id}', function (int $id, Request $request) {
    $old_email = $request->input('old_email');
    $new_email = $request->input('new_email');
    $password = $request->input('password');
    $fname = $request->input('fname');
    $lname = $request->input('lname');
    $contact = $request->input('contact');
    $weight = $request->input('weight');
    $height = $request->input('height');
    $religion = $request->input('religion');
    $health = implode("|", $request->input('health'));

    if ($old_email != $new_email) {
        $used_email = DB::table('accounts')->select('email')->where('email', $new_email)->first();
        if ($used_email) {
            return response()->json(['message' => 'Email is already used.'], 409);
        }
    }

    DB::table('accounts')
        ->where('id', $id)
        ->update([
            'email' => $new_email,
            'password' => sha1($password),
            'user_fname' => $fname,
            'user_lname' => $lname,
            'contact' => $contact,
            'weight' => $weight,
            'height' => $height,
            'religion' => $religion,
            'complication' => $health
        ]);

    return Redirect::to("/user/{$id}");
});

# Get all workouts per category
Route::get('/user/workout/{category}', function ($category) {
    $workouts = DB::table('workout as w')
                    ->join('workout_levels as w_lvl', 'w.id', '=', 'w_lvl.workout_id')
                    ->select('w.name as category', 'w.id as workout_id', 'w_lvl.id', 'w_lvl.name', 'w_lvl.level', 'w_lvl.week')
                    ->orderBy('w.name', 'w_lvl.name', 'w_lvl.level')
                    ->get()->toArray();

    $filter_workouts = function ($cat, $level) use ($workouts) {
        $filtered = array_values(array_filter($workouts, function ($wo) use ($cat, $level) {
            return $wo->category == $cat && $wo->level == $level;
        }));

        return array_map(function ($wo) {
            return array(
                'name' => $wo->name,
                'week' => $wo->week,
                'workout_list' => array_map( function ($wl) { return $wl->name; }, 
                                    DB::table('workout_list')
                                        ->select('name')
                                        ->where([
                                            ['week_id', '=', $wo->week],
                                            ['level_id', '=', $wo->level],
                                            ['workout_id', '=', $wo->workout_id],
                                            ['status', '=', 1]
                                        ])->get()->toArray()
                                    )
            );
        }, $filtered);
    };

    $categories = array(
        "full_body" => "Full Body",
        "arm" => "Arm",
        "leg" => "Leg",
        "core" => "Core",
        "butt" => "Butt",
        "cardio" => "Cardio",
        "dieting" => "Weight Loss and Dieting"
    );

    $category_levels = array(
        "beginner" => $filter_workouts($categories[$category], 1),
        "intermediate" => $filter_workouts($categories[$category], 2),
        "advanced" => $filter_workouts($categories[$category], 3)
    );

    return response()->json(array($category => $category_levels));
});

# Get workout schedule
Route::get('/user/{id}/schedule', function (int $id) {
    $user_workout = DB::table('user_workout as uw')
                        ->join('workout as w', 'w.id', '=', 'uw.workout_id')
                        ->select('name', 'workout_id', 'workout_level as level', 'uw.created_at')
                        ->where([
                            ['uw.status', '=', 1],
                            ['uw.user_id', '=', $id]
                        ])
                        ->first();
    
    if ($user_workout) {
        $workout_recs = DB::table('workout_levels')
                            ->select('name', 'week', 'level', 'workout_id')
                            ->where([
                                ['workout_id', '=', $user_workout->workout_id],
                                ['level', '=', $user_workout->level]
                            ])
                            ->get();
        $rec_count = count($workout_recs);
        $create_date = new DateTime($user_workout->created_at);
        $expire_date = $create_date->add(new DateInterval("P{$rec_count}D"))->format('Y-m-d');

        $workouts = array();
        foreach ($workout_recs as $wo) {
            $wout_list = DB::table('workout_list')
                            ->select('name')
                            ->where([
                                ['level_id', '=', $wo->level],
                                ['week_id', '=', $wo->week],
                                ['workout_id', '=', $wo->workout_id],
                                ['status', '=', 1]
                            ])
                            ->get()->toArray();

            $wo->workout_list = array_map(function ($w) { return $w->name; }, $wout_list);
            array_push($workouts, $wo);
        }

        $resp = response()->json([
            'workouts' => $workouts,
            'user_workout' => array(
                'name' => $user_workout->name,
                'level' => $user_workout->level,
                'workout_id' => $user_workout->workout_id
            ),
            'expire_date' => $expire_date
        ]);
    } else {
        $resp = response()->json([], 404);
    }

    return $resp;
});

# End workout schedule
Route::post('/user/{id}/schedule/end', function (int $id) {
    DB::table('user_workout')
        ->where('user_id', $id)
        ->update(['status' => 0]);
    
    return response()->json(['message' => 'Workout ended successfully.']);
});

# Enroll workout class
Route::post('/user/{id}/myclass', function (int $id, Request $request) {
    $class_id = $request->input('class_id');
    $trainer_id = $request->input('trainer_id');
    $enrolled = DB::table('user_class')
                    ->select('id')
                    ->where([
                        ['class_id', '=', $class_id],
                        ['trainer_id', '=', $trainer_id],
                        ['accounts_id', '=', $id],
                        ['status', '=', 1]
                    ])->first();

    if ($enrolled) {
        $resp = response()->json(['message' => 'You are already enrolled in this class.'], 409);
    } else {
        DB::table('user_class')
            ->insert([
                'accounts_id' => $id,
                'class_id' => $class_id,
                'trainer_id' => $trainer_id,
                'status' => 1,
                'feedback_status' => 0
            ]);
        $resp = response()->json(['message' => 'Enrolled successfully.']);
    }

    return $resp;
});

# Get user's workout classes
Route::get('/user/{id}/myclass', function (int $id) {
    $classes = DB::table('class as c')
                ->join('user_class as uc', 'uc.class_id', '=', 'c.id')
                ->join('trainer as t', 't.id', '=', 'uc.trainer_id')
                ->select('c.id', 'class_name', 'class_desc', 'start_time', 'end_time', 'day', 'uc.trainer_id as trainer_id',
                    DB::raw("CONCAT(trainer_fname, ' ', trainer_lname) as trainer_name"))
                ->where([
                    ['uc.status', '=', 1],
                    ['c.status', '=', 1],
                    ['uc.accounts_id', '=', $id]
                ])
                ->get()->toArray();

    if ($classes) {
        $resp = response()->json(array_map(function ($c) {
            return array(
                'id' => $c->id,
                'name' => $c->class_name,
                'desc' => $c->class_desc,
                'day' => $c->day,
                'start_time' => $c->start_time,
                'end_time' => $c->end_time,
                'trainer' => array(
                    'id' => $c->trainer_id,
                    'name' => $c->trainer_name,
                )
            );
        }, $classes));
    } else {
        $resp = response()->json([], 404);
    }

    return $resp;
});

# End workout class
Route::post('/user/{id}/myclass/end', function (int $id, Request $request) {
    $class_id = $request->input('class_id');
    $trainer_id = $request->input('trainer_id');
    DB::table('user_class')
        ->where([
            ['accounts_id', '=', $id],
            ['class_id', '=', $class_id],
            ['trainer_id', '=', $trainer_id]
        ])
        ->update(['status' => 0]);

    return response()->json(['message' => 'Workout class ended successfully.']);
});

# Add meal plan
Route::post('/user/{id}/mealplan', function (int $id, Request $request) {
    $active_mealplan = DB::table('meal_plan')
                            ->select('user_id')
                            ->where([
                                ['status', '=', 1],
                                ['user_id', '=', $id]
                            ])
                            ->first();
    if ($active_mealplan) {
        DB::table('meal_plan')
            ->where('user_id', $id)
            ->update(['status' => 0]);
    }

    $gender = $request->input('gender');
    $weight = $request->input('weight');
    $height = $request->input('height');
    $target_weight = $request->input('target_weight');
    $physical_activity = $request->input('physical_activity');
    $religion = $request->input('religion');
    $foods_avoid = $request->input("foods_avoid");
    $health = $request->input("health");
    $food_preference = $request->input("food_preference");
    $medical_info = '';

    if ($physical_activity) {
        switch ($physical_activity) {
            case 'bodybuilding':
                $category = 'weight_gain';
                break;
            case 'dieting':
                $category = 'weight_loss';
                break;
            default:
                $category = 'weight_maintenance';
                break;
        }
    } else {
        if ($target_weight > $weight) {
            $category = 'weight_gain';
            $physical_activity = 'bodybuilding';
        } elseif ($target_weight < $weight) {
            $category = 'weight_loss';
            $physical_activity = 'dieting';
        } else {
            $category = 'weight_maintenance';
            $physical_activity = 'cardio';
        }
    }

    # get available foods
    $to_r_predicate = function ($arr) { 
        return $arr ? implode(' and ', array_map(function ($e) {
            return "restrictions not like '%{$e}%'";
        }, $arr)) : NULL;
    };

    $r_religion = in_array($religion, ['islam', 'iglesia', 'hindu']) ? "restrictions not like '%{$religion}%'" : NULL;
    $r_foods_avoid = $to_r_predicate($foods_avoid);
    $r_health = $to_r_predicate($health);
    $restrictions = implode(' and ', array_filter([$r_religion, $r_foods_avoid, $r_health]));

    Log::debug("Restrictions predicate: {$restrictions}");

    $food_pref_predicate = $food_preference ? implode(' or ', array_map(function ($f) {
        return "food_preference like '%{$f}%'";
    }, $food_preference)) : NULL;

    $base_query = "select id, name, serving, food_group, time_served, category, program " .
                  "from foods where program like '%{$physical_activity}%' and category like '%{$category}%'";
    if ($food_pref_predicate) {
        $base_query = $base_query . " and ({$food_pref_predicate})";
    }

    if ($restrictions) {
        $available_foods = DB::select(DB::raw($base_query . " and {$restrictions}"));
    } else {
        $available_foods = DB::select(DB::raw($base_query));
    }
    Log::debug("Number of available foods: " . count($available_foods));

    $get_random_food = function ($time_served) use ($available_foods) {
        $filtered = array_values(
            array_filter($available_foods, function ($f) use ($time_served) { 
                return $f->time_served == $time_served; 
        }));
        $meal_foods_size = rand(4, 5);
        $meal_foods = [];

        while (count($meal_foods) < $meal_foods_size) {
            $idx = rand(0, count($filtered) - 1);           // generate random index
            if (in_array($filtered[$idx], $meal_foods)) {   // ensure there are no duplicates
                continue;
            } else {
                array_push($meal_foods, $filtered[$idx]);   // add to meals
            }
        }

        return array_map(function ($meal) {
            return array('name' => $meal->name, 'serving' => $meal->serving);
        }, $meal_foods);
    };

    $end_date = date('Y-m-d', strtotime(' + 14 days'));

    $meals = [];
    foreach (range(1, 14) as $day) {
        array_push($meals, array(
            'day' => $day,
            'expire_date' => date('Y-m-d', strtotime(" + {$day} days")),
            'breakfast' => $get_random_food('breakfast'),
            'snack' => $get_random_food('snack'),
            'lunch' => $get_random_food('lunch'),
            'dinner' => $get_random_food('dinner')
        ));
    }

    // insert new meal plan and get generated ID
    $mealplan_id = DB::table('meal_plan')
                    ->insertGetId([
                        'user_id' => $id,
                        'gender' => $gender,
                        'height' => $height,
                        'weight' => $weight,
                        'target_weight' => $target_weight,
                        'physical_activity' => $physical_activity,
                        'religion' => $religion,
                        'health' => implode('|', $health),
                        'foods_avoid' => implode('|', $foods_avoid),
                        'medical_info' => $medical_info,
                        'meals' => json_encode($meals),
                        'status' => 1,
                        'remove_status' => 0
                    ]);

    return response()->json([
        'id' => $mealplan_id,
        'user_id' => $id,
        'program' => $physical_activity,
        'expire_date' => $end_date,
        'meals' => $meals
    ], 201);
});

# Get meal plan
Route::get('/user/{id}/mealplan', function (int $id) {
    $mealplan = DB::table('meal_plan')
                    ->select('id', 'user_id', 'physical_activity', 'meals', 'created_at')
                    ->where('user_id', $id)
                    ->where('status', 1)
                    ->first();

    if ($mealplan) {
        $create_date = date_create($mealplan->created_at);
        $end_date = $create_date->add(new DateInterval('P14D'))->format('Y-m-d');
        $resp = response()->json([
            'id' => $mealplan->id,
            'user_id' => $mealplan->user_id,
            'program' => $mealplan->physical_activity,
            'expire_date' => $end_date,
            'meals' => json_decode($mealplan->meals)
        ]);
    } else {
        $resp = response()->json(['message' => 'No active meal plan schedule found.'], 404);
    }

    return $resp;
});

# End meal plan
Route::post('/user/{id}/mealplan/end', function (int $id) {
    DB::table('meal_plan')
        ->where('user_id', $id)
        ->update(['status' => 0]);

    return response()->json(['message' => 'Meal plan ended.']);
});

# Add feedback to workout class
Route::post('/user/{id}/feedback', function (int $id, Request $request) {
    $class_id = $request->input('class_id');
    $trainer_id = $request->input('trainer_id');
    $trainer_criteria1 = $request->input('trainer_criteria1');
    $trainer_criteria2 = $request->input('trainer_criteria2');
    $trainer_criteria3 = $request->input('trainer_criteria3');
    $trainer_msg = $request->input('trainer_msg');
    $class_criteria1 = $request->input('class_criteria1');
    $class_criteria2 = $request->input('class_criteria2');
    $class_msg = $request->input('class_msg');

    DB::table('feedbacks')
        ->insert([
            'user_id' => $id,
            'class_id' => $class_id,
            'trainer_id' => $trainer_id,
            'trainer_criteria1' => $trainer_criteria1,
            'trainer_criteria2' => $trainer_criteria2,
            'trainer_criteria3' => $trainer_criteria3,
            'trainer_msg' => $trainer_msg,
            'class_criteria1' => $class_criteria1,
            'class_criteria2' => $class_criteria2,
            'class_msg' => $class_msg
        ]);

    DB::table('user_class')
        ->where([
            ['accounts_id', '=', $id],
            ['class_id', '=', $class_id],
            ['trainer_id', '=', $trainer_id]
        ])
        ->update(['feedback_status' => 1]);

    return response()->json(['message' => 'Feedback added successfully.']);
});

# Get pending feedbacks
Route::get('/user/{id}/feedback/pending', function (int $id) {
    $pending = DB::table('class as c')
        ->join('user_class as uc','uc.class_id', '=', 'c.id')
        ->join('trainer as t', 't.id', '=', 'uc.trainer_id')
        ->select('c.id', 'class_name', 'class_desc', 'start_time', 'end_time', 'day',
            'uc.trainer_id as trainer_id', DB::raw("CONCAT(trainer_fname, ' ', trainer_lname) as trainer_name"))
        ->where([
            ['uc.status', '=', 0],
            ['uc.feedback_status', '=', 0],
            ['uc.accounts_id', '=', $id]
        ])
        ->get()->toArray();

    if ($pending) {
        $resp = response()->json(array_map(function ($c) {
            return array(
                'id' => $c->id,
                'name' => $c->class_name,
                'desc' => $c->class_desc,
                'day' => $c->day,
                'start_time' => $c->start_time,
                'end_time' => $c->end_time,
                'trainer' => array(
                    'id' => $c->trainer_id,
                    'name' => $c->trainer_name
                )
            );
        }, $pending));
    } else {
        $resp = response()->json([], 404);
    }

    return $resp;
});

// Admin - add meal plan
Route::post('/admin/mealplan', function () {
    // TODO implementation
});